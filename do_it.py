import subprocess
import argparse
import contextlib
import os.path
import sys
@contextlib.contextmanager
def chdir(where):
    location = os.getcwd()
    os.chdir(where)
    yield
    os.chdir(location)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--package", required=True)
    parser.add_argument("--version", required=True)
    parser.add_argument("--use-languages", action="append")
    parser.add_argument("--numpy", action="store_true")
    args = parser.parse_args()
    package = args.package
    version = args.version
    if args.use_languages:
        use_langs = " ".join(args.use_languages)
    else:
        use_langs = "# none"
    numpy = '.include "../../math/py-numpy/buildlink3.mk"' if args.numpy else ""
    letter = package[0]

    py_name = "py-{0}".format(package)
    if not os.path.exists(py_name):
        subprocess.check_call("cp -r py-template {0}".format(py_name), shell=True)

    with open(os.path.join(py_name, "Makefile"), mode="w") as fd:
        fd.write("""
# $NetBSD$

DISTNAME=	{package}-{version}
PKGNAME=	${{PYPKGPREFIX}}-${{DISTNAME}}
CATEGORIES=	mdanielson devel
MASTER_SITES=	${{MASTER_SITE_PYPI:={letter}/{package}/}}

MAINTAINER=	INSERT_YOUR_MAIL_ADDRESS_HERE
COMMENT=	TODO: Short description of the package
#LICENSE=	# TODO: (see mk/license.mk)

USE_LANGUAGES=	{langs}


.include "../../lang/python/distutils.mk"
.include "../../lang/python/application.mk"
{numpy}
.include "../../mk/bsd.pkg.mk"
""".format(
    py_name=py_name,
    letter=letter,
    version=version,
    package=package,
    langs=use_langs,
    numpy=numpy
))
    pyversion = "38"
    with chdir(py_name):
        subprocess.check_call("rm -f PLIST", shell=True)
        subprocess.check_call("touch PLIST", shell=True)
        subprocess.check_call("bmake PYTHON_VERSION_DEFAULT={} clean".format(pyversion), shell=True)
        subprocess.check_call("bmake PYTHON_VERSION_DEFAULT={} mdi".format(pyversion), shell=True)
        subprocess.call("bmake PYTHON_VERSION_DEFAULT={} package".format(pyversion), shell=True)
        subprocess.check_call("bmake PYTHON_VERSION_DEFAULT={} print-PLIST > PLIST".format(pyversion), shell=True)
        subprocess.check_call("bmake PYTHON_VERSION_DEFAULT={} clean".format(pyversion), shell=True)
        subprocess.call("bmake PYTHON_VERSION_DEFAULT={} package".format(pyversion), shell=True)

if __name__ == "__main__":
    main()

